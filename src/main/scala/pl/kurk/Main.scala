package pl.kurk

import org.apache.spark.sql._

object Main extends App {

  override def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder()
      .config("spark.executor.memory", "2g")
      .config("spark.driver.memory", "4g")
      .config("spark.master", "local")
      .getOrCreate()
    val packageName = "anime.stackexchange.com"

    val maxDownVotes = findLocationWithMaxDownVotes(packageName)
    val maxUpVotes = findLocationWithMaxUpVotes(packageName)
    val tagsPop = tagPopularityV2(packageName)

    println(s"Najwięcej pozytywnych użytkowników jest z ${maxUpVotes.getString(0)} ")
    println(s"Najwięcej negatywnych użytkowników jest z ${maxDownVotes.getString(0)} ")
    println("Ranking popularności tagów: ")
    tagsPop.foreach(f => println(s"${f.getString(0)} -- ${f.getLong(1)} wyświetleń"))
  }

  //workaround for the bug https://github.com/databricks/spark-xml/issues/92
  def getDataframe(packageName: String, filename: String, rootTag: String)(implicit spark: SparkSession): DataFrame =
    spark.read
      .format("com.databricks.spark.xml")
      .option("rowTag", rootTag)
      .load(s"$packageName/$filename")
      .selectExpr("explode(row) as row")

  def findLocationWithMaxUpVotes(packageName: String)(implicit spark: SparkSession): Row = {
    val userDf = getDataframe(packageName, "Users.xml", "users")
    val rowsUpVotes = userDf.select("row._Location", "row._UpVotes")
    val maxUpVotes = rowsUpVotes.groupBy("_Location").max("_UpVotes").toDF("Location", "UpVotes")
    val orderedMaxUpVotes = maxUpVotes.orderBy(maxUpVotes("UpVotes").desc)
    orderedMaxUpVotes.first()
  }

  def findLocationWithMaxDownVotes(packageName: String)(implicit spark: SparkSession): Row = {
    val userDf = getDataframe(packageName, "Users.xml", "users")
    val rowsDownVotes = userDf.select("row._Location", "row._DownVotes")
    val maxDownVotes = rowsDownVotes.groupBy("_Location").max("_DownVotes").toDF("Location", "DownVotes")
    val orderedMaxDownVotes = maxDownVotes.orderBy(maxDownVotes("DownVotes").desc)
    orderedMaxDownVotes.first()
  }

  def tagPopularity(packageName: String)(implicit spark: SparkSession): Array[Row] = {
    val postsDf = getDataframe(packageName, "Tags.xml", "tags")
    val rows = postsDf.select("row._TagName", "row._Count")
    val orderedList = rows.orderBy(rows("_Count").desc).toDF("Tag", "Views").limit(10)
    orderedList.collect()
  }

  /**
    * Same solution as tagPopularity but calculated from Posts.xml
    */
  def tagPopularityV2(packageName: String)(implicit spark: SparkSession): Array[Row] = {
    import spark.implicits._
    val postsDf = getDataframe(packageName, "Posts.xml", "posts")
    val customEncoder: Encoder[(String, Long)] = Encoders.tuple(Encoders.STRING, Encoders.scalaLong)
    val rows = postsDf.select("row._Tags").where("_Tags is not null")
    val list = rows.flatMap(row => {
      val regex = "(?<=\\<)[^>]+(?=\\>)".r
      if (!row.anyNull)
        regex.findAllIn(row.getString(0)).map(f => (f, 1L))
      else List()
    })(customEncoder).rdd
    val viewCountList = list.reduceByKey(_ + _).toDF("Tag", "Views")
    val orderedList = viewCountList.orderBy(viewCountList("Views").desc).limit(10)
    orderedList.collect()
  }


}
